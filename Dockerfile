FROM python:3.7

LABEL maintainer "Foo Bar <foo@bar.xyz>"

RUN apt-get update

RUN mkdir /work
WORKDIR /work
COPY . /work

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["python"]
