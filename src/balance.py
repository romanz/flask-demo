"""Simple flask API server."""
from flask import Flask, Response, jsonify, abort
from collections import defaultdict

app = Flask(__name__)
db = defaultdict(float)

@app.route('/<username>/add/<float:delta>', methods=['POST'])
def add(username, delta):
    db[username] += delta
    return jsonify(msg='success')

@app.route('/<username>/sub/<float:delta>', methods=['POST'])
def sub(username, delta):
    db[username] -= delta
    return jsonify(msg='success')

@app.route('/<username>')
def balance_handler(username):
    balance = db.get(username)
    if balance is None:
        abort(Response(f'user {username} not found', status=404))
    return jsonify(username=username, balance=db[username])


if __name__ == '__main__':
    # ONLY FOR TESTING, DON'T RUN IN PRODUCTION!!!
    app.run(debug=True, host='0.0.0.0', port=5000)
