"""Simple flask API server."""
from flask import Flask, Response, jsonify, abort
import os
import json
import requests

app = Flask(__name__)

balance_api = os.environ['BALANCE_API']


def call_api(endpoint):
    response = requests.get(balance_api + '/' + endpoint)
    if response.ok:
        return response.json()
    else:
        abort(response.status_code)


@app.route('/')
def root_handler():
    return 'Welcome to Flask!'

@app.route('/user/<username>')
def user_handler(username):
    return jsonify(msg='Welcome {}!'.format(username))

@app.route('/user/<username>/balance')
def balance_handler(username):
    return jsonify(**call_api(username))

if __name__ == '__main__':
    # ONLY FOR TESTING, DON'T RUN IN PRODUCTION!!!
    app.run(debug=True, host='0.0.0.0', port=5000)
