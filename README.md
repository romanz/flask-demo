Build and test a single container:
```
$ docker build --tag flask-demo:latest .
$ docker run -p 5000:5000 flask-demo:latest src/app.py
$ docker run -p 5001:5000 flask-demo:latest src/balance.py
```


Define a service with mutliple containers:
```bash
$ docker-compose build
$ docker-compose up/down
```


$ curl localhost:5000/
Welcome to Flask!

$ curl localhost:5000/user/X
{
  "msg": "Welcome X!"
}

$ curl localhost:5000/user/X/balance
{
  "balance": 123.45,
  "username": "X"
}

$ curl localhost:5001/A/add/1.1 -X POST
{
  "msg": "success"
}
$ curl localhost:5001/A
{
  "balance": 1.1,
  "username": "A"
}
$ curl localhost:5001/B
user not found
